#include <TDirectory.h>
#include <TFile.h>
#include <TNtupleD.h>
#include <TROOT.h>
#include <TSpline.h>
#include <TVectorT.h>
#include <ROOT/TDataFrame.hxx>

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <functional>
#include <initializer_list>
#include <iterator>
#include <limits>
#include <map>
#include <random>
#include <string>
#include <vector>

#include <fmt/container.h>
#include <fmt/format.h>

#include <range/v3/all.hpp>

#include <gsl/gsl_math.h>
#include <gsl/gsl_statistics_double.h>

#include "digitizer.h"
#include "fpexception.h"

using namespace ROOT::Experimental;
using namespace ranges;

double get_wp(TFile* CDI, const std::string& algorithm, const std::string& jet_collection,
              const std::string& wp_type, double wp, double pT) {
    std::string dir_name =
          fmt::format("{}/{}/{}BEff_{:2.0f}", algorithm, jet_collection, wp_type, wp);
    TDirectoryFile* dir = dynamic_cast<TDirectoryFile*>(CDI->Get(dir_name.c_str()));
    if (!dir) {
        fmt::print(stderr, "Directory {} does not exist\n");
        std::exit(1);
    }
    TVectorT<float>* cutval = dynamic_cast<TVectorT<float>*>(dir->Get("cutvalue"));
    if (cutval) {
        float value = (*cutval)[0];
        return value;
    }
    else { // must have a cutprofile
        TSpline3* cutprofile = dynamic_cast<TSpline3*>(dir->Get("cutprofile"));
        double    value      = cutprofile->Eval(pT);
        return value;
    }
}

void process(const std::string& algorithm, const std::string& jet_collection,
             const std::vector<const char*>& files, TFile* output_file, TFile* CDI,
             const std::string& wp_type) {
    const bool          fromCDI = CDI && !(wp_type == "FlatEff");
    std::vector<double> working_points;
    if (!fromCDI) {
        working_points = {30., 50., 60., 70., 77., 85., 90.};
    }
    else {
        working_points = {60., 70., 77., 85.};
    }
    const std::string output_vars = fmt::format(
          "bin_center:{}%:{}_err:{}_lr:{}_lr_err:{}_lr_prop_err:{}_p:{}_p_err:{}_p_prop_err",
          fmt::join(working_points, "%:"), fmt::join(working_points, "_err:"),
          fmt::join(working_points, "_lr:"), fmt::join(working_points, "_lr_err:"),
          fmt::join(working_points, "_lr_prop_err:"), fmt::join(working_points, "_p:"),
          fmt::join(working_points, "_p_err:"), fmt::join(working_points, "_p_prop_err:"));

    auto     output_name = fmt::format("{}_{}", wp_type, jet_collection);
    TNtupleD output(output_name.c_str(), output_name.c_str(), output_vars.c_str());
    // Bins between 30 GeV and 2000 GeV of width 10 GeV
    static const Digitizer digitizer(30 * 1e3, 2000 * 1e3, 10 * 1e3, 20 * 1e3);
    // print in blue
    fmt::print("\x1b[34mProcessing {}/{}/{}\x1b[0m\n", algorithm, jet_collection, wp_type);
    std::fflush(stdout);
    if (!output_file->GetDirectory(algorithm.c_str())) {
        output_file->mkdir(algorithm.c_str());
    }
    output_file->cd(algorithm.c_str());

    auto frame = TDataFrame(fmt::format("{}/{}/rawData", algorithm, jet_collection), files);

    fmt::print("Defining binning...\n");
    std::fflush(stdout);
    auto binned =
          frame.Filter([](double pT) { return (pT > 25 * 1e3) && (pT < 2000 * 1e3); }, {"pT"})
                .Define("pT_bin", digitizer, {"pT"});

    using proxy_type = decltype(binned.Take<double>("weight"));
    std::vector<proxy_type> true_bs_proxies;
    true_bs_proxies.reserve(digitizer.max_index());
    std::vector<proxy_type> false_bs_proxies;
    false_bs_proxies.reserve(digitizer.max_index());
    std::vector<proxy_type> lights_proxies;
    lights_proxies.reserve(digitizer.max_index());

    // for every bin, define and save the proxies
    for (int i = 0; i < digitizer.max_index(); ++i) {
        auto data          = binned.Filter([i](int pT_bin) { return pT_bin == i; }, {"pT_bin"});
        auto true_bs_proxy = data.Filter([](char truth) { return truth == 'b'; }, {"truth"})
                                   .Take<double>("weight");
        auto false_bs_proxy = data.Filter([](char truth) { return truth != 'b'; }, {"truth"})
                                    .Take<double>("weight");
        auto lights_proxy = data.Filter([](char truth) { return truth == 'L'; }, {"truth"})
                                  .Take<double>("weight");
        true_bs_proxies.push_back(true_bs_proxy);
        false_bs_proxies.push_back(false_bs_proxy);
        lights_proxies.push_back(lights_proxy);
    }

    // force binning
    fmt::print("Running binning event loop (slow)...\n");
    auto throwaway = true_bs_proxies[0]->size();

    // max_index includes the upper overflow bin, compensating for 0 indexing
    fmt::print("Calculating ({} bins)...\n", digitizer.max_index());
    std::fflush(stdout);
    for (int i = 0; i < digitizer.max_index(); ++i) {
        // save cursor at end
        fmt::print("Index {:<3} (central pT = {:>5} GeV): \x1b[33m\x1b[s", i, digitizer[i] / 1e3);
        fmt::print("selecting...");
        std::fflush(stdout);
        std::vector<double> true_bs  = *(true_bs_proxies[i]);
        std::vector<double> false_bs = *(false_bs_proxies[i]);
        std::vector<double> lights   = *(lights_proxies[i]);

        int num_true  = true_bs.size();
        int num_false = false_bs.size();
        int num_light = lights.size();

        if (true_bs.size() < 10) {
            fmt::print("\x1b[u\x1b[K\x1b[0m... fewer than 10 (or no) true bs\n");
            continue;
        }
        else if (lights.size() == 0) {
            fmt::print("\x1b[u\x1b[K\x1b[0m... no light jets\n");
            continue;
        }
        fmt::print("\x1b[u\x1b[Ksorting true bs...");
        std::fflush(stdout);
        // split true_bs into 10 bins for error calculation
        const int                    n_subsamples = fromCDI ? 0 : 10;
        std::vector<std::vector<double>> true_b_bins(n_subsamples);
        if(!fromCDI) {
            std::mt19937 rng(15); // fixed seed for repeatability
            shuffle(true_bs, rng);
            bool flag = false;
            for (int j = 0; j < n_subsamples; ++j) {
                true_b_bins[j] = true_bs | view::drop(j) | view::stride(n_subsamples);
                if (true_b_bins[j].size() <= 0) {
                    fmt::print("\x1b[u\x1b[K\x1b[0m... no true bs for subsample {}\n", j + 1);
                    flag = true;
                    continue;
                }
                sort(true_b_bins[j]);
            }
            if (flag) continue;
        }
        sort(true_bs);
        // determine working points
        std::vector<double> wp_thresholds;
        std::vector<double> wp_errs;
        std::vector<double> light_rejs;
        std::vector<double> light_rej_errs;
        std::vector<double> light_rej_prop_errs;
        std::vector<double> purities;
        std::vector<double> purity_errs;
        std::vector<double> purity_prop_errs;
        wp_thresholds.reserve(working_points.size());
        light_rejs.reserve(working_points.size());
        light_rej_errs.reserve(working_points.size());
        purities.reserve(working_points.size());
        purity_errs.reserve(working_points.size());
        fmt::print("\x1b[u\x1b[Kcalculating working points...");
        std::fflush(stdout);
        for (auto working_point : working_points) {
            // calculate threshold
            double threshold = 0.;
            if (!fromCDI) {
                threshold = gsl_stats_quantile_from_sorted_data(true_bs.data(), 1, true_bs.size(),
                                                                1. - (working_point / 100.));
            }
            else {
                threshold = get_wp(CDI, algorithm, jet_collection, wp_type, working_point,
                                   digitizer[i] / 1e3);
            }
            auto pred = [threshold](double weight) { return weight >= threshold; };

            // calculate results
            int    true_pass  = count_if(true_bs, pred);
            int    false_pass = count_if(false_bs, pred);
            int    light_pass = count_if(lights, pred);
            double light_pass_err;
            double light_rej;
            double light_rej_err;
            double purity = true_pass / double(true_pass + false_pass);
            double purity_err;

            if (std::isinf(purity)) {
                purity     = std::nan("");
                purity_err = std::nan("");
            }
            else {
                double total_pass = true_pass + false_pass;
                double num_total  = num_true + num_false;
                // Fractional errors
                double true_pass_err =
                      std::sqrt(true_pass * (1 - true_pass / num_true)) / true_pass;
                double total_pass_err =
                      std::sqrt(total_pass * (1 - total_pass / num_total)) / total_pass;
                purity_err = (true_pass_err + total_pass_err) * purity;
            }

            if (light_pass > 0) {
                // This is a fractional error
                light_pass_err = std::sqrt(light_pass * (1 - light_pass / num_light)) / light_pass;
                light_rej      = num_light / light_pass;
                light_rej_err  = light_pass_err * light_rej;
            }
            else {
                light_rej     = std::numeric_limits<double>::infinity();
                light_rej_err = 0;
            }

            // Error from miscalculating trial thresholds
            std::vector<double> trial_thresholds;
            std::vector<double> trial_lrs;
            std::vector<double> trial_purities;
            for (auto&& bin : true_b_bins) {
                // If we're reading from a CDI, just use the original threshold
                double trial_thresh =
                      fromCDI ? threshold
                              : gsl_stats_quantile_from_sorted_data(bin.data(), 1, bin.size(),
                                                                    1. - (working_point / 100.));
                auto trial_pred = [trial_thresh](double weight) { return weight >= trial_thresh; };

                int true_pass  = count_if(true_bs, trial_pred);
                int false_pass = count_if(false_bs, trial_pred);
                int light_pass = count_if(lights, trial_pred);

                double trial_purity =
                      true_pass == 0 ? std::nan("") : true_pass / double(true_pass + false_pass);
                double trial_lr = light_pass == 0 ? std::numeric_limits<double>::infinity()
                                                  : num_light / light_pass;
                trial_thresholds.push_back(trial_thresh);
                trial_lrs.push_back(trial_lr);
                trial_purities.push_back(trial_purity);
            }
            double threshold_err = 0.;
            double light_rej_prop_err = 0.;
            double purity_prop_err = 0.;
            if(!fromCDI) {
                threshold_err = gsl_stats_sd_with_fixed_mean(trial_thresholds.data(), 1,
                                                             trial_thresholds.size(), threshold);
                light_rej_prop_err =
                      gsl_stats_sd_with_fixed_mean(trial_lrs.data(), 1, trial_lrs.size(), light_rej);
                light_rej_prop_err =
                      std::sqrt(gsl_pow_2(light_rej_err) + gsl_pow_2(light_rej_prop_err));
                purity_prop_err = gsl_stats_sd_with_fixed_mean(trial_purities.data(), 1,
                                                               trial_purities.size(), purity);
                purity_prop_err = std::sqrt(gsl_pow_2(purity_err) + gsl_pow_2(purity_prop_err));
            }

            wp_thresholds.push_back(threshold);
            wp_errs.push_back(threshold_err);
            light_rejs.push_back(light_rej);
            light_rej_errs.push_back(light_rej_err);
            light_rej_prop_errs.push_back(light_rej_prop_err);
            purities.push_back(purity);
            purity_errs.push_back(purity_err);
            purity_prop_errs.push_back(purity_prop_err);
        }

        fmt::print("\x1b[u\x1b[Kstoring...");
        std::fflush(stdout);
        std::vector<double> output_data;
        output_data.reserve(working_points.size() * 8 + 1);
        output_data.push_back(digitizer[i] / 1e3);
        copy(wp_thresholds, back_inserter(output_data));
        copy(wp_errs, back_inserter(output_data));
        copy(light_rejs, back_inserter(output_data));
        copy(light_rej_errs, back_inserter(output_data));
        copy(light_rej_prop_errs, back_inserter(output_data));
        copy(purities, back_inserter(output_data));
        copy(purity_errs, back_inserter(output_data));
        copy(purity_prop_errs, back_inserter(output_data));
        output.Fill(output_data.data());
        fmt::print("\x1b[u\x1b[K\x1b[0m... done\n");
        std::fflush(stdout);
    }
    fmt::print("Writing...\n");
    output.Write();
}

int main(int argc, char* argv[]) {
    ROOT::EnableImplicitMT();

    if (argc == 1 || !std::strcmp(argv[1], "--help") || (!std::strcmp(argv[1], "-c") && argc < 4)) {
        fmt::print("Usage: pT_dep [-c cdi_file] filenames\n");
        return 0;
    }

    TFile*                   CDI = nullptr;
    std::vector<std::string> wp_types{"FlatEff"};
    if (!std::strcmp(argv[1], "-c")) {
        CDI = new TFile(argv[2]);
        wp_types.push_back("FixedCut");
        wp_types.push_back("Hyb");
    }
    std::vector<const char*> files(argv + (CDI ? 3 : 1), argv + argc);
    TFile*                   output = new TFile("processed.root", "RECREATE");

    for (const std::string& jet_collection : {"AntiKt4EMPFlowJets", "AntiKt4EMTopoJets"}) {
        for (const std::string& algo : {"MV2c10" /*, "MV2c10mu", "MV2c10rnn" */}) {
            for (const std::string& wp_type : wp_types) {
                process(algo, jet_collection, files, output, CDI, wp_type);
            }
        }
    }
    output->Close();
    delete output;
    return 0;
}
