#pragma once
#pragma STDC FENV_ACCESS ON

#include <cfenv>
#include <cstdlib>
#include <signal.h>
#include <fmt/format.h>

void catch_fpexcept(int sig) {
    int exception = fegetexcept();
    fmt::print(stderr, "\nFloating point exception raised: {:#b}\n", exception);
    std::feclearexcept(FE_ALL_EXCEPT);
    std::exit(1);
}


void enable_fpe_handler() {
    feenableexcept(FE_ALL_EXCEPT);
    signal(SIGFPE, catch_fpexcept);
}

