#include "digitizer.h"

#include <fmt/format.h>
#include <range/v3/all.hpp>

#include <functional>
#include <limits>
#include <stdexcept>
#include <vector>

using namespace ranges;

Digitizer::Digitizer(double start, double end, double width, double min) : min(min) {
    if (start >= end) throw std::invalid_argument("Digitizer constructor: start >= end");
    if (min >= start) throw std::invalid_argument("Digitizer constructor: min >= start");
    if (std::remainder(end - start, width) != 0.) {
        throw std::invalid_argument(
              fmt::format("Digitizer constructor: (end - start) not a multiple of width\n"
                          "start = {}, end = {}, width = {}",
                          start, end, width));
    }

    // Include start and end
    bin_edges = view::linear_distribute(start, end, (end - start) / width + 1);

    bin_centers = view::concat(view::single((start + min) / 2),
                               view::zip_with([](double a, double b) { return (a + b) / 2; },
                                              bin_edges, view::drop(bin_edges, 1)),
                               view::single(std::numeric_limits<double>::infinity()));

    fmt::print("CONSTRUCTED DIGITIZER -- UNDERFLOW BIN CENTER: {:.0f} MeV, FIRST BIN CENTER: "
               "{:.0f} MeV, "
               "LAST BIN CENTER: {:.0f} MeV\n\n",
               bin_centers[0], bin_centers[1], *(bin_centers.rbegin() + 1));
}

int Digitizer::operator()(double value) const {
    auto found = upper_bound(bin_edges, value);
    if (found == bin_edges.end()) {
        throw std::logic_error(
              fmt::format("Digitizer: value {} not found, min ({}) set incorrectly", value, min));
    }

    return (found - bin_edges.begin());
}

double Digitizer::operator[](int index) const {
    try {
        return bin_centers.at(index);
    }
    catch (std::out_of_range) {
        throw std::invalid_argument(fmt::format("Digitizer: invalid bin index {}", index));
    }
}

int Digitizer::max_index() const { return bin_centers.size() - 1; }
