#pragma once
#include <vector>

class Digitizer {
public:
  Digitizer(double start, double end, double width, double min = 0.);
  Digitizer(const Digitizer&) = default;
  Digitizer& operator=(const Digitizer&) = delete;

  int operator()(double value) const;
  double operator[](int index) const;
  int max_index() const;
private:
  double min;
  std::vector<double> bin_edges;
  std::vector<double> bin_centers;
};

